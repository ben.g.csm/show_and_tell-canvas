const elements = Object.fromEntries(
	[
		`code`,
		`fillNextCode`
	].map(el => [el,document.getElementById(el)])
);
const canvas = document.getElementById(`canvas`)




const code = [
	...[
		"canvas.width = canvas.height = 500;",
		"const ctx = canvas.getContext('2d');",
		"\nctx.clearRect(0,0,canvas.width,canvas.height); \/\/x,y,width,height",
		"\nctx.strokeStyle = '#8997cc';\nctx.strokeRect(60,60,250,250); \/\/x,y,width,height",
		"\nctx.fillStyle = '#de99b5';\nctx.fillRect(30,30,250,250); \/\/x,y,width,height",
		"\nctx.beginPath();\nctx.rect(0,0,250,250); \/\/x,y,width,height\nctx.fillStyle='#b7ba02';\nctx.fill();\nctx.strokeStyle='brown';\nctx.stroke();",
		"\nctx.font = '20px Arial';",
		"\nconst gradient = ctx.createLinearGradient(30, 30, 200, 50);\ngradient.addColorStop(0, 'white');\ngradient.addColorStop(1, 'blue');\nctx.fillStyle = gradient",
		"\nctx.fillText('I LOVEEEE canvas!',30,30);"
	].map((el,index,arr) => arr.slice(0,index+1).join("\n")),
	...[
		"canvas.width = 500;\ncanvas.height = 500*img.height/img.width;\nconst ctx = canvas.getContext('2d');\nctx.clearRect(0,0,canvas.width,canvas.height);",
		"\nctx.drawImage(img,0,0,500,500*img.height/img.width); \/\/x,y,width,height",
		"\nconst imageData = ctx.getImageData(0,0,canvas.width,canvas.height);",
		"const pixels = imageData.data",
		"\nfor (let i=0; i < pixels.length; i +=4) {\n\tconst red = pixels[i];\n\tconst green = pixels[i+1];\n\tconst blue = pixels[i+2]\n\t \/\/ Note that alpha is the fourth channel",
		"\tif(green < (red+blue)/1.5) pixels[i+3] = 0;\n\t",
		"}",
		"ctx.putImageData(imageData, 0, 0);"

	].map((el,index,arr) => arr.slice(0,index+1).join("\n"))
];

const runCode = () => {
	(new Function(elements.code.value))()
}
const fillNextCode = () => {
	elements.code.value = code.shift();
	if (!code.length) {
		elements.fillNextCode.disabled = true;
	}
}